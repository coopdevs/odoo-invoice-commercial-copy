{
    'name': "Odoo Preserve commercial in invoice copy",
    'version': '12.0.0.0.1',
    'depends': ['account'],
    'author': "Coopdevs Treball SCCL",
    'website': 'https://coopdevs.org',
    'category': "Cooperative management",
    'description': """
    Odoo Preserve commercial in invoice copy.
    """,
    "license": "AGPL-3",
    'data': [
    ],
}
